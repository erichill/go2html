package go2html

import (
	"bytes"
	"io"
	"strings"
)

func (t *Tag) String() string {
	buf := &bytes.Buffer{}
	t.Render(buf, 0)
	return buf.String()
}

func (t *Tag) Render(w io.Writer, level int) {
	const SPACES_PER_LEVEL = 4

	p1 := strings.Repeat(" ", SPACES_PER_LEVEL*level)
	io.WriteString(w, p1)

	// Write the opening tag
	io.WriteString(w, "<")
	io.WriteString(w, t.Name)

	// Classes
	if len(t.Classes) > 0 {
		io.WriteString(w, " class=\"")
		io.WriteString(w, strings.Join(t.Classes, " "))
		io.WriteString(w, "\"")
	}

	// Attributes
	for k, v := range t.Attributes {
		io.WriteString(w, " ")
		io.WriteString(w, k)
		if len(v) > 0 {
			io.WriteString(w, "=\"")
			io.WriteString(w, v)
			io.WriteString(w, "\"")
		} else {
			// Only write empty values for non-prefixed attributes
			if !strings.Contains(k, "-") {
				io.WriteString(w, "=\"\"")
			}
		}
	}

	// If there's no content, close the tag and return
	if len(t.Content) == 0 && len(t.Tags) == 0 {
		// Some tags are special and should not be self-closed even if empty
		if t.Name == "script" || t.Name == "td" || t.Name == "div" || t.Name == "span" {
			io.WriteString(w, ">")
			goto CloseTag
		}

		// Self-close tag
		io.WriteString(w, " />\n")
		return
	}

	io.WriteString(w, ">")

	if len(t.Tags) > 0 {
		io.WriteString(w, "\n")
	}
	for _, tag := range t.Tags {
		tag.Render(w, level+1)
	}

	// If there's any text content, pad it
	if len(t.Content) > 0 {
		lines := strings.Split(strings.TrimSpace(t.Content), "\n")

		// Single line of text goes inline in the HTML
		if len(lines) == 1 {
			io.WriteString(w, lines[0])
			// Fast close the tag and return
			io.WriteString(w, "</")
			io.WriteString(w, t.Name)
			io.WriteString(w, ">\n")
			return
		}

		io.WriteString(w, "\n") // Multiple lines start on next line
		p2 := strings.Repeat(" ", SPACES_PER_LEVEL*(level+1))
		for _, line := range lines {
			io.WriteString(w, p2)
			io.WriteString(w, line)
			io.WriteString(w, "\n")
		}
	}

	io.WriteString(w, p1)
CloseTag:
	io.WriteString(w, "</")
	io.WriteString(w, t.Name)
	io.WriteString(w, ">\n")
}
