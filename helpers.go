package go2html

func Html(children ...*Tag) *Tag {
	t := NewNamedTag("html").Children(children...)
	t.Attributes["xmlns"] = "http://www.w3.org/1999/xhtml"
	return t
}

func Head(children ...*Tag) *Tag {
	return NewNamedTag("head").Children(children...)
}

func Body(children ...*Tag) *Tag {
	return NewNamedTag("body").Children(children...)
}

func Anchor(href string) *Tag {
	return NewNamedTag("a").Attr("href", href)
}

func Meta() *Tag {
	return NewNamedTag("meta")
}

func MetaViewport(content string) *Tag {
	t := NewNamedTag("meta")
	t.Attributes["name"] = "viewport"
	t.Attributes["content"] = content
	return t
}

func Title(title string) *Tag {
	t := NewNamedTag("title")
	t.Content = title
	return t
}

func MetaCompatible() *Tag {
	t := NewNamedTag("meta")
	t.Attributes["http-equiv"] = "X-UA-Compatible"
	t.Attributes["content"] = "IE=edge"
	return t
}

func MetaContentType() *Tag {
	t := NewNamedTag("meta")
	t.Attributes["http-equiv"] = "content-type"
	t.Attributes["content"] = "text/html; charset=utf-8"
	return t
}

func LinkStylesheet(url string) *Tag {
	t := NewNamedTag("link")
	t.Attributes["rel"] = "stylesheet"
	t.Attributes["type"] = "text/css"
	t.Attributes["href"] = url
	return t
}

func Script(script string) *Tag {
	t := NewNamedTag("script")
	t.Attributes["type"] = "text/javascript"
	t.Content = script
	return t
}

func ScriptUrl(url string) *Tag {
	t := NewNamedTag("script")
	t.Attributes["type"] = "text/javascript"
	t.Attributes["src"] = url
	return t
}

func Div(children ...*Tag) *Tag {
	return NewNamedTag("div").Children(children...)
}

func Span(children ...*Tag) *Tag {
	return NewNamedTag("span").Children(children...)
}

func Ul(children ...*Tag) *Tag {
	return NewNamedTag("ul").Children(children...)
}

func Li(text string) *Tag {
	t := NewNamedTag("li")
	t.Content = text
	return t
}

func H1(text string) *Tag {
	t := NewNamedTag("h1")
	t.Content = text
	return t
}

func H2(text string) *Tag {
	t := NewNamedTag("h2")
	t.Content = text
	return t
}

func H3(text string) *Tag {
	t := NewNamedTag("h3")
	t.Content = text
	return t
}

func H4(text string) *Tag {
	t := NewNamedTag("h4")
	t.Content = text
	return t
}

func Hr() *Tag {
	return NewNamedTag("hr")
}

func THead(columnLabels ...string) *Tag {
	tr := NewNamedTag("tr")
	for _, s := range columnLabels {
		tr.Children(NewNamedTag("th").Text(s))
	}
	return NewNamedTag("thead").Children(tr)
}

func TBodyBind(dataSet string, columnNames ...string) *Tag {
	tr := NewNamedTag("tr")
	for _, s := range columnNames {
		tr.Children(NewNamedTag("td").Attr("data-bind", "text: "+s))
	}
	return NewNamedTag("tbody").Attr("data-bind", "foreach: "+dataSet).Children(tr)
}

func Tr(children ...*Tag) *Tag {
	return NewNamedTag("tr").Children(children...)
}

func Table(head *Tag, body *Tag) *Tag {
	return NewNamedTag("table").Children(head, body)
}

func P(text string) *Tag {
	t := NewNamedTag("p")
	t.Content = text
	return t
}

func Form(children ...*Tag) *Tag {
	return NewNamedTag("form").Children(children...)
}

func Input(id string) *Tag {
	return NewNamedTag("input").Id(id)
}

func Label(text string) *Tag {
	return NewNamedTag("label").Text(text)
}

func Button(text string) *Tag {
	return NewNamedTag("button").Text(text)
}

func Image(src string, alt string) *Tag {
	return NewNamedTag("image").Attr("src", src).Attr("alt", alt)
}

func Nav(children ...*Tag) *Tag {
	return NewNamedTag("nav").Children(children...)
}

func Section(children ...*Tag) *Tag {
	return NewNamedTag("section").Children(children...)
}
