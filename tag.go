package go2html

type Tag struct {
	Name       string
	Classes    []string
	Attributes map[string]string
	Tags       []*Tag
	Content    string
}

func NewTag() *Tag {
	return &Tag{
		Attributes: make(map[string]string),
	}
}

func NewNamedTag(name string) *Tag {
	return &Tag{
		Attributes: make(map[string]string),
		Name:       name,
	}
}

func (t *Tag) Attr(name string, value string) *Tag {
	t.Attributes[name] = value
	return t
}

func (t *Tag) Class(name ...string) *Tag {
	t.Classes = append(t.Classes, name...)
	return t
}

func (t *Tag) Id(name string) *Tag {
	return t.Attr("id", name)
}

func (t *Tag) Text(text string) *Tag {
	t.Content = text
	return t
}

func (t *Tag) Children(children ...*Tag) *Tag {
	t.Tags = append(t.Tags, children...)
	return t
}
