package go2html

import (
	"testing"
)

func TestSimpleForm(t *testing.T) {
	doc := Html(
		Head(
			Title("ELab Idle"),
			MetaCompatible(),
			MetaContentType(),
			MetaViewport("width=device-width, initial-scale=1.0, maximum-scale=1.0"),
			LinkStylesheet("https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono"),
			ScriptUrl("https://code.jquery.com/jquery-3.2.1.min.js"),
		),
		Body(
			Div(
				P("Hello World"),
			).Class("content"),
			Script("$(pageReady());"),
		),
	)

	t.Log(doc.String())
}

func TestTag(t *testing.T) {
	tag := Div(
		P("One"),
		P("Two"),
		Div(
			P("Three"),
			P("Four"),
		),
	).Class("container")
	t.Log(tag.String())
}
