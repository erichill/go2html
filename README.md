# go2html
Inspired by j2html, this library provides a simple framework for generating properly indented HTML from Go code.
Please **do not** use this to generate HTML live on every page refresh.  It is not optimized for performance at all.
We're using it internally to generate static HTML forms.  

## Usage
Code speaks for itself.

```go
package main
    
import (
    . "github.com/ericzhill/go2html" // Import at the root namespace
    "bytes"
)    
    
func main() {
    doc := Html(
        Head(
            Title("ELab Idle"),
            MetaCompatible(),
            MetaContentType(),
            MetaViewport("width=device-width, initial-scale=1.0, maximum-scale=1.0"),
            LinkStylesheet("https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono"),
            ScriptUrl("https://code.jquery.com/jquery-3.2.1.min.js"),
        ),
        Body(
            Div(
                P("Hello World"),
            ).Class("content"),
            Script("$(pageReady());"),
        ),
    )

    out := &bytes.Buffer{}
    doc.Render(out, 0)

    fmt.Println(out.String())
}
```

Resulting output...

```html
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>ELab Idle</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    </head>
    <body>
        <div class="content">
            <p>Hello World</p>
        </div>
        <script type="text/javascript">$(pageReady());</script>
    </body>
</html>
```

Not all HTML tags are available as this is just a convenience package for me. Feel free to submit a pull request 
with more features or to fix bugs.

## Design

The base construct is the Tag.

```go
type Tag struct {
	Name       string
	Classes    []string
	Attributes map[string]string
	Tags       []*Tag
	Content    string
}
```

The name denotes the type of HTML element, e.g. div, p, span, etc.
   
Classes are zero or more class names to be added to the class attribute.
  
Attributes contain zero or more attribute key/value pairs.  Yes, this means that you can't have the 
same attribute twice with two different values.  I didn't want to over-complicate it.  Sorry.
  
Tags contain zero or more children that will be rendered under the current node.

Content contains any text content that will be placed inside the tag.  This is especially useful for
script tags that contain javascript.

## Simpler examples

### Simple tag
Go
```go
tag := P("Hello World")
tag.String()
```

HTML
```html
<p>Hello World</p>
```

### Class
Go
```go
tag := P("Hello World").Class("banner")
t.Log(tag.String())
```

HTML
```html
<p class="banner">Hello World</p>
```

### Attributes
Go
```go
tag := P("")
tag.Class("banner")
tag.Attr("data-bind", "source")
t.Log(tag.String())
```

HTML
```html
<p class="banner" data-bind="source" />
```

### Nesting
Go
```go
tag := Div(
    P("One"),
    P("Two"),
    Div(
        P("Three"),
        P("Four"),
    ),
).Class("container")
t.Log(tag.String())
```

HTML
```html
<div class="container">
    <p>One</p>
    <p>Two</p>
    <div>
        <p>Three</p>
        <p>Four</p>
    </div>
</div>
```
